#! usr/bin/env Python3
import csv
import os
from typing import Union

import pandas as pd
from pandas.core.frame import DataFrame

def try_convert(input :str) -> Union[int, float, str]:
    if input == '':
        return 0
    try:
        return int(input)
    except:
        try:
            return float(input)
        except:
            return input

def convert_panstars_list_to(file_name :str) -> None:
    os.chdir('/home/denis/Panstars')
    with open('total.list','r') as source, open(file_name,'tw') as goal:
        reader = csv.reader(source, delimiter='|')
        writer = csv.writer(goal, delimiter=',')
        for line in reader:
            line = [try_convert(cell.strip()) for cell in line]
            writer.writerow(line)

def get_pandas_df_with_grades(file_name :str) -> DataFrame:
    os.chdir('/home/denis/Panstars')
    total = pd.read_csv(file_name, delimiter=',')
    grades = []
    for i in total.index:
        good  = total.loc[i]['good_votes']
        mean  = total.loc[i]['acceptable_votes']
        bad   = total.loc[i]['unsuitable_votes']
        votes = total.loc[i]['total_votes']
        if votes != 0:
            estate = (4/3*good + 2/3*mean - 2/3*bad)/(votes) - 1/3
            grades.append(estate)
        else:
            grades.append(-1.0)
    total['grade'] = grades
    return total

def get_panstars_name(projcell :int, skycell :int, number=0, type='small') -> str:
    os.chdir('/home/denis/Panstars')
    folder_name = 'Tile_'+f'{projcell:04}'+'_'+f'{skycell:03}'
    if type == 'small':
        sml_0 = 'candidate_'+f'{number:01}'+'_r.fits'
        current_file = os.path.join('new_photometry', folder_name, sml_0)
    elif type == 'big':
        big_0 = 'candidate_bigger_'+f'{number:01}'+'_r.fits'
        current_file = os.path.join('new_photometry', folder_name, big_0)
    elif type == 'json':
        jsn_0 = 'candidate_'+f'{number:01}'+'_r.json'
        current_file = os.path.join('new_photometry', folder_name, jsn_0)
    else:
        raise KeyError
    return current_file

######################################################3
