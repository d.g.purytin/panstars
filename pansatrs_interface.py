#! usr/bin/env Python3
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

from astropy.visualization import astropy_mpl_style, LogStretch, SqrtStretch
from astropy.visualization import AsymmetricPercentileInterval
from astropy.visualization.mpl_normalize import ImageNormalize
from astropy.io import fits

from panstars_dataframe import get_panstars_name

from PIL.Image import fromarray

def panstars_picture(projcell :int, skycell :int, galaxy_number=0,
                     interval=[0,100], norm='none') -> Figure:
    plt.style.use(astropy_mpl_style)
    os.chdir('/home/denis/Panstars')

    current_file = get_panstars_name(projcell, skycell, galaxy_number, 'small')
    
    image_data = fits.getdata(current_file, ext=0)

    interval = AsymmetricPercentileInterval(*interval)
    interval_min, interval_max = interval.get_limits(image_data)

    if norm == 'none':
        norm = ImageNormalize(vmin=interval_min, vmax=interval_max)
    elif norm == 'sqrt':
        norm = ImageNormalize(vmin=0, vmax=interval_max, stretch=SqrtStretch())
    elif norm == 'log':
        norm = ImageNormalize(vmin=0, vmax=interval_max, stretch=LogStretch())
    else:
        raise KeyError

    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    im = ax.imshow(image_data, origin='lower', norm=norm)
    fig.colorbar(im)
    return fig

def add_markup_to_current_fig(projcell :int, skycell :int, galaxy_number=0):
    ax = plt.gca()
    x_min, x_max = ax.get_xlim()
    y_min, y_max = ax.get_ylim()

    json_file = get_panstars_name(projcell, skycell, galaxy_number, 'json')

    os.chdir('/home/denis/Panstars')
    with open(json_file, "r") as read_file:
        desereal = json.load(read_file)

    x_img = desereal['X_IMAGE']
    y_img = desereal['Y_IMAGE']
    theta_img = desereal['THETA_IMAGE']

    pi = 3.141592653
    x = [0, x_max]
    y = [y_img-(x_img-x_min)*np.tan(theta_img*pi/180),
         y_img+(x_max-x_img)*np.tan(theta_img*pi/180)]

    plt.plot(x, y, color='cyan', linestyle='dotted', linewidth=2)
    plt.scatter(x_img, y_img, color='blue', marker='x')

    ax.set_xlim([x_min, x_max])
    ax.set_ylim([y_min, y_max])

#############################################################################

def scale_image(projcell :int, skycell :int, galaxy_number=0, size=64): 
    os.chdir('/home/denis/Panstars')
    current_file = get_panstars_name(projcell, skycell, galaxy_number, 'small')
    image_data = fits.getdata(current_file, ext=0)
    
    return np.array(fromarray(image_data).resize((size,size)))